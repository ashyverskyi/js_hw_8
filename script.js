// TASK 1

function isPalindrome(str) {
    const re = /[^а-яёa-z0-9]/g;
    const lowRegStr = str.toLowerCase().replace(re, '');
    const reverseStr = lowRegStr.split('').reverse().join('');
    return reverseStr === lowRegStr;
}

console.log(isPalindrome("- Валентина, выпьем?! - Не откажусь"));
console.log(isPalindrome("А врет, стерва!"));

// TASK 2

function stringLengthChecker(textInput, textLength) {
    return textInput.length <= textLength;
}

console.log(stringLengthChecker("Check my length", 15));

// TASK 3

const birthYear = parseInt(prompt("Enter your birth year in a 4-digit format:"));
const birthMonth = parseInt(prompt("Enter your birth month using a number from 1 to 12:"));
const birthDay = parseInt(prompt("Enter your birth day using a number from 1 to 31:"));

function calculateAge(birthYear, birthMonth, birthDay) {
    const currentDate = new Date();
    const currentYear = currentDate.getFullYear();
    const currentMonth = currentDate.getMonth();
    const currentDay = currentDate.getDate();

    let age = currentYear - birthYear;

    if (currentMonth < birthMonth || (currentMonth === birthMonth && currentDay < birthDay)) {
        age--;
    }

    return age;
}

const userAge = calculateAge(birthYear, birthMonth, birthDay);

if (userAge > 120) {
    alert("Well, well, aren't we spinning tales of time travel now, Mr. " + userAge + " years old?");
} else {
    alert("Your are " + userAge + " years old.");
}








